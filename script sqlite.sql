CREATE TABLE "utilisateurs" (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom VARCHAR(25) NOT NULL,
    prenom VARCHAR(25) NOT NULL,
    ddn DATE NOT NULL,
    genre TINYINT(1) NOT NULL,
    taille TINYINT(3) NOT NULL,
    poids TINYINT(3) NOT NULL,
    email VARCHAR(50) NOT NULL,
    mdp VARCHAR(68) NOT NULL
);

CREATE TABLE "activites" (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    date DATE NOT NULL,
    description VARCHAR(255) NOT NULL,
    utilisateur_id INTEGER NOT NULL,
    FOREIGN KEY(utilisateur_id) REFERENCES utilisateurs(id)
);

CREATE TABLE "donnees" (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    temps TIME NOT NULL,
    frequence_cardio TINYINT NOT NULL,
    latitude FLOAT(9) NOT NULL,
    longitude FLOAT(9) NOT NULL,
    altitude SMALLINT(4) NOT NULL,
    activite_id INTEGER NOT NULL,
    FOREIGN KEY(activite_id) REFERENCES activite(id)
);
