# Projet SportTrack

## Namespaces
L'application est structurée suivant le modèle MVC2
Les différents composantes de l'application sont regroupées sous des namespaces pour une meilleure structure. Un autoloader inclut automatiquement les fichiers des classes importées.
 - **\\App\\** Namespace racine de l'application
 - **\\App\\Core\\** Contient toutes les classes maitresses de l'application comme le controleur de façade ou la gestion des routes
 - **\\App\\Config\\** Contient les fichiers de configurations (routes, constantes, ...)
  - **\\App\\Model\\** Contient les classes modèles de la BDD (Utilisateur, Activite, ...)
  - **\\App\\Model\\Tool\\** Classes utilitaires des modèles (factory et calcul de distances)
  - **\\App\\Pages\\** Regroupe les controleur et les vues
  - **Le dossier static** contient les CSS et les images

## Étapes lors d'une requête HTTP
Lorsque l'`index.php` est appellé:
1. Un autoloader est enregistré pour importer automatiquement les classes

2. Le Frontcontroleur est instancié et sa méthode `render()` est appellée pour déclencher le rendu HTML
    * 2.1. Le FrontController demande à la Classe `Router` la route correspondant à l'url demandée. Si aucune route ne correspond, l'exception `E404` est déclenchée.
    * 2.2. Le FrontController instancie la classe controleur associé à la route et lance la méthode définie dans la route (un controleur a plusieurs méthodes pour gérer plusieurs routes contrairement dans le sujet ou un controleur gérait une seule route à travers la méthode `match()`);
    * 2.3. Le controleur doit importer sa vue lui même, le résultat de la vue est stocké dans la variable `$_SESSION['html_body']`

3. L'html est généré:
    - En injectant le la variable `$_SESSION['html_title']` dans la balise `<title>`
    - En affichant les notifications stockées dans le tableau `$_SESSION['notifications']`
    - Suivi du corps de la page généré par la vue contenue dans la variable `$_SESSION['html_body']`
