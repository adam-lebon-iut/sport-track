<?php
namespace App\DAO;

use PDO;

class SqliteConnexion
{

    /**
     * Instance du singleton de la classe
     *
     * @var SqliteConnexion
     */
    private static $instance;

    /**
     * Sqlite3 connexion
     *
     * @var PDO
     */
    private $connexion;

    public function __construct()
    {
        $this->connexion = new PDO('sqlite:' . ROOT_DIR . 'db.sqlite3');
        $this->connexion->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, FALSE); 
        $this->connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function findWhere(string $tableName, string $where = '', array $values = [])
    {
        $where = !empty($where) ? "WHERE $where" : '';

        $statement = $this->connexion->prepare("SELECT * FROM $tableName $where;");
        /*if ($values !== null) {
            foreach ($values as $name => $value) {
                if (is_array($value) && isset($value[1])) {
                    $statement->bindParam(":$name", $value[0], $value[1]);
                } else {
                    $statement->bindParam(":$name", $value);
                    print('<br>');
                    print ("bind :$name => $value");
                }
            }
        }*/
        $statement->execute($values);
        return $statement->fetchAll();
    }

    public function insert(string $tableName, array $fields) : int
    {
        $statement = $this->connexion->prepare(
            "INSERT INTO " . $tableName .
            '(' . join(', ', array_keys($fields)) . ') VALUES ' .
            '(' . join(', ', array_map(
                function ($field) {return ":$field";},
                array_keys($fields)
            ))
            . ')'
        );

        foreach ($fields as $name => $value) {
            if (is_array($value) && isset($value[1])) {
                $statement->bindParam(":$name", $value[0], $value[1]);
            } else {
                $statement->bindParam(":$name", $value);
            }
        }

        $statement->execute();

        return $this->connexion->lastInsertId();
    }

    public function update(string $tableName, int $id, array $fields)
    {
        $statement = $this->connexion->prepare(

            "UPDATE " . $tableName . " SET " .
            join(', ', array_map(
                function ($field) {return "$field = :$field";},
                array_keys($fields)
            )) .
            " WHERE id = " . $id
        );

        foreach ($fields as $name => $value) {
            if (is_array($value) && isset($value[1])) {
                $statement->bindParam(":$name", $value[0], $value[1]);
            } else {
                $statement->bindParam(":$name", $value);
            }
        }

        return $statement->execute();
    }

    public function delete(string $tableName, int $id)
    {
        $statement = $this->connexion->prepare(
            "DELETE FROM " . $tableName . " WHERE id = " . $id
        );

        return $statement->execute();
    }

    public function deleteWhere(string $tableName, string $where = '', array $values = [])
    {
        $where = !empty($where) ? "WHERE $where" : '';

        $statement = $this->connexion->prepare("DELETE FROM $tableName $where;");
        if ($values !== null) {
            foreach ($values as $name => $value) {
                if (is_array($value) && isset($value[1])) {
                    $statement->bindParam(":$name", $value[0], $value[1]);
                } else {
                    $statement->bindParam(":$name", $value);
                }
            }
        }
        $statement->execute();
    }

    /**
     * Permet de récupérer l'instance du singleton de la classe
     *
     * @return SqliteConnexion instance de la classe
     */
    public static function getInstance(): SqliteConnexion
    {
        if (!isset(self::$instance)) {
            self::$instance = new SqliteConnexion();
        }
        return self::$instance;
    }
}
