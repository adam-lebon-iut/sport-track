<?php

namespace App\DAO;

use DateTime;
use PDO;
use App\DAO\SqliteConnexion;
use App\Model\Utilisateur;

class UtilisateurDAO
{
    private static $dao;

    private function __construct()
    {}

    final public static function getInstance()
    {
        if (!isset(self::$dao)) {
            self::$dao = new UtilisateurDAO();
        }
        return self::$dao;
    }

   /**
    * Undocumented function
    *
    * @param string $where
    * @param array $values
    * @return array<Utilisateur>
    */
    final public function findWhere(string $where = '', array $values = []): array
    {
        return array_map(
            function (array $row) {
                $instance = new Utilisateur();
                $instance->id = intval($row['id']);
                $instance->nom = $row['nom'];
                $instance->prenom = $row['prenom'];
                $instance->ddn = new DateTime($row['ddn']);
                $instance->genre = intval($row['genre']);
                $instance->taille = intval($row['taille']);
                $instance->poids = intval($row['poids']);
                $instance->email = $row['email'];
                $instance->setPasswordHash($row['mdp']);
                return $instance;
            },
            SqliteConnexion::getInstance()->findWhere('utilisateurs', $where, $values)
        );
    }

    final public function findAll()
    {
        return $this->findWhere();
    }

    final public function insert(Utilisateur $user)
    {
        $user->id = SqliteConnexion::getInstance()->insert('utilisateurs', [
            'email'  => [$user->email,                PDO::PARAM_STR],
            'prenom' => [$user->prenom,               PDO::PARAM_STR],
            'nom'    => [$user->nom,                  PDO::PARAM_STR],
            'ddn'    => [$user->ddn->format('Y-m-d'), PDO::PARAM_STR],
            'genre'  => [$user->genre,                PDO::PARAM_INT],
            'taille' => [$user->taille,               PDO::PARAM_INT],
            'poids'  => [$user->poids,                PDO::PARAM_INT],
            'email'  => [$user->email,                PDO::PARAM_STR],
            'mdp'    => [$user->getPasswordHash(),    PDO::PARAM_STR],
        ]);
    }

    public function delete(Utilisateur $user)
    {
        SqliteConnexion::getInstance()->delete('utilisateurs', $user->id);
    }

    public function update(Utilisateur $user)
    {
        SqliteConnexion::getInstance()->update('utilisateurs', $user->id, [
            'email'  => [$user->email,                PDO::PARAM_STR],
            'prenom' => [$user->prenom,               PDO::PARAM_STR],
            'nom'    => [$user->nom,                  PDO::PARAM_STR],
            'ddn'    => [$user->ddn->format('Y-m-d'), PDO::PARAM_STR],
            'genre'  => [$user->genre,                PDO::PARAM_INT],
            'taille' => [$user->taille,               PDO::PARAM_INT],
            'poids'  => [$user->poids,                PDO::PARAM_INT],
            'email'  => [$user->email,                PDO::PARAM_STR],
            'mdp'    => [$user->getPasswordHash(),    PDO::PARAM_STR],
        ]);
    }
}
