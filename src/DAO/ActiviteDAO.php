<?php

namespace App\DAO;

use PDO;
use App\DAO\SqliteConnexion;
use App\Model\Activite;

class ActiviteDAO
{
    private static $dao;

    private function __construct()
    {}

    final public static function getInstance()
    {
        if (!isset(self::$dao)) {
            self::$dao = new ActiviteDAO();
        }
        return self::$dao;
    }

    /**
     * Undocumented function
     *
     * @param string $where
     * @param array $values
     * @return array<Activite>
     */
    final public function findWhere(string $where = '', array $values = []): array
    {
        return array_map(
            function ($row) {
                $instance = new Activite();
                $instance->id = intval($row['id']);
                $instance->date = \DateTime::createFromFormat('d/m/Y', $row['date']);
                $instance->description = $row['description'];
                $instance->utilisateurId = intval($row['utilisateur_id']);
                return $instance;
            },
            SqliteConnexion::getInstance()->findWhere('activites', $where, $values)
        );
    }

    final public function findAll()
    {
        return $this->findWhere();
    }

    final public function insert(Activite $activite)
    {
        $activite->id = SqliteConnexion::getInstance()->insert('activites', [
            'date' => [$activite->date->format('d/m/Y'), PDO::PARAM_STR],
            'description' => [$activite->description, PDO::PARAM_STR],
            'utilisateur_id' => [$activite->utilisateurId, PDO::PARAM_INT]
        ]);
    }

    public function delete(int $id)
    {
        // On supprime les données associés
        SqliteConnexion::getInstance()->deleteWhere('donnees', 'activite_id = :id', ['id' => $id]);
        // Puis l'activité
        SqliteConnexion::getInstance()->delete('activites', $id);
    }

    public function update(Activite $activite)
    {
        SqliteConnexion::getInstance()->update('activites', $activite->id, [
            'date' => [$activite->date->format('d/m/Y'), PDO::PARAM_STR],
            'description' => [$activite->description, PDO::PARAM_STR],
            'utilisateur_id' => [$activite->utilisateurId, PDO::PARAM_INT]
        ]);
    }
}
