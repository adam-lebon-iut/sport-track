<?php

namespace App\DAO;

use DateTime;
use PDO;
use App\DAO\SqliteConnexion;
use App\Model\Donnee;

class DonneeDAO
{
    private static $dao;

    private function __construct()
    {}

    final public static function getInstance(): DonneeDAO
    {
        if (!isset(self::$dao)) {
            self::$dao = new DonneeDAO();
        }
        return self::$dao;
    }

   /**
    * Undocumented function
    *
    * @param string $where
    * @param array $values
    * @return array<Donnee>
    */
    final public function findWhere(string $where = '', array $values = []): array
    {
        return array_map(
            function($line) {
                $instance = new Donnee();
                
                $instance->id = intval($line['id']);
                $instance->longitude = floatval($line['longitude']);
                $instance->latitude = floatval($line['latitude']);
                $instance->altitude = floatval($line['altitude']);
                $instance->time = \DateTime::createFromFormat('H:i:s', $line['temps']);
                $instance->cardioFrequency = intval($line['frequence_cardio']);
                $instance->activite_id = intval($line['activite_id']);
                return $instance;
            },
            SqliteConnexion::getInstance()->findWhere('donnees', $where, $values)
        );
    }

    final public function findAll()
    {
        return $this->findWhere(NULL, NULL);
    }

    final public function insert(Donnee $data)
    {
        $data->id = SqliteConnexion::getInstance()->insert('donnees', [
            'latitude'         => [$data->latitude,              PDO::PARAM_STR],
            'longitude'        => [$data->longitude,             PDO::PARAM_STR],
            'altitude'         => [$data->altitude,              PDO::PARAM_STR],
            'temps'            => [$data->time->format('H:i:s'), PDO::PARAM_STR],
            'frequence_cardio' => [$data->cardioFrequency,       PDO::PARAM_STR],
            'activite_id'      => [$data->activiteId,            PDO::PARAM_INT],
        ]);
    }

    public function delete(Donnee $data)
    {
        SqliteConnexion::getInstance()->delete('donnees', $data->id);
    }

    public function update(Donnee $data)
    {
        SqliteConnexion::getInstance()->update('donnees', $data->id, [
            'latitude'        => [$data->latitude,              PDO::PARAM_STR],
            'longitude'       => [$data->longitude,             PDO::PARAM_STR],
            'altitude'        => [$data->altitude,              PDO::PARAM_STR],
            'time'            => [$data->time->format('Y-m-d'), PDO::PARAM_STR],
            'cardioFrequency' => [$data->cardioFrequency,       PDO::PARAM_STR],
        ]);
    }
}
