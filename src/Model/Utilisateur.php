<?php

namespace App\Model;

use App\DAO\ActiviteDAO;
use App\Model\EUserGender;
use DateTime;

/**
 * Class Model d'un utilisateur
 * @author Adam LE BON <lebon.adam@gmail.com>
 */
class Utilisateur
{
    /**
     * Identifiant unique en BDD
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $nom;

    /**
     * @var string
     */
    public $prenom;

    /**
     * Date de naissance
     * @var DateTime
     */
    public $ddn;

    /**
     * Genre
     * @var EUserGender
     */
    public $genre;

    /**
     * Taille en cm
     * @var int
     */
    public $taille;

    /**
     * @var int
     */
    public $poids;

    /**
     * @var string
     */
    public $email;

    /**
     * Mot de passe hashé en SHA256
     * @var string
     */
    private $mdp;

    /**
     * Permet de récupérer les activités de l'utilisateur courant
     *
     * @return Array<Utilisateur>
     */
    public function getActivities(): Array
    {
        return ActiviteDAO::getInstance()->findWhere('utilisateur_id = :uid', ['uid' => $this->id]);
    }

    /**
     * Vérifie la validité du mot de passe passé en paramètre
     *
     * @param string $password Mot de passe à vérifier
     * @return boolean Exactitude du mot de passe
     */
    public function checkPassword(string $password): boolean
    {
        return sha1($password) === $this->mdp;
    }
    
    public function setPassword(string $password)
    {
        $this->mdp = sha1($password);
    }

    /**
     * Getter du hash du mot de passe
     *
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->mdp;
    }

    public function setPasswordHash(string $hash)
    {
        $this->mdp = $hash;
    }

    /**
     * Définie un nouveau mot de passe en vérifiant les paramètres de sécurité
     * - Minimum de 8 charactères
     * - Contient au moins une majuscule
     * - Contient un moins un chiffre
     *
     * @return boolean Renvoie TRUE si le mot de passe à bien été sauvegardé
     */
    public function validatePassword(string $newPassword): boolean
    {
        if (strlen($newPassword) >= 8 &&
            preg_match("/[A-Z]/", $newPassword) !== 0 &&
            preg_match("/[0-9]/", $newPassword) !== 0
        ) {
            return false;
        } else {
            $this->mdp = $password;
            return true;
        }
    }
}
