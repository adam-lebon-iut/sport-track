<?php

namespace App\Model;

use App\Model\Tool\CalculDistanceImpl;
use App\DAO\DonneeDAO;

class Activite
{

    public $id;
    public $date;
    public $description;
    public $utilisateurId;

    /**
     * Undocumented function
     *
     * @return Array<Donnee>
     */
    public function getDonnees(): array
    {
        return DonneeDAO::getInstance()->findWhere('activite_id = :aid', ["aid" => $this->id]);
    }

    public function getDistance(): float
    {
        return CalculDistanceImpl::getInstance()->calculDistanceTrajet($this->getDonnees());
    }

    public function getAverageCardio(): int
    {
        $sum = 0;
        $donnees = $this->getDonnees();
        foreach ($donnees as $data) {
            $sum += $data->cardioFrequency;
        }
        return floor($sum / count($donnees));
    }

    public function getMinCardio(): int
    {
        $min = INF;
        foreach ($this->getDonnees() as $data) {
            if ($data->cardioFrequency < $min) {
                $min = $data->cardioFrequency;
            }
        }
        return $min;
    }

    public function getMaxCardio(): int
    {
        $max = 0;
        foreach ($this->getDonnees() as $data) {
            if ($data->cardioFrequency > $max) {
                $max = $data->cardioFrequency;
            }
        }
        return $max;
    }

    public function getStartTime(): string
    {
        
    }

    public function getDuration(): string
    {

    }

    public function getEndTime(): string
    {
        
    }

}
