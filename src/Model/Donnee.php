<?php

namespace App\Model;

use App\Model\Tool\CalculDistanceImpl;

class Donnee {

    public $id;
    public $latitude;
    public $longitude;
    public $altitude;
    public $time;
    public $cardioFrequency;
    public $activiteId;

}

?>