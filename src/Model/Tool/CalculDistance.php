<?php

namespace App\Model\Tool;

use App\Model\Tool\ICalculDistance;

class CalculDistance implements ICalculDistance {

    private static $instance = NULL;

    const EARTH_RADIUS = 6371e3;

    public function calculDistance2PointsGPS(float $lat1, float $long1, float $lat2, float $long2): float {
        $latFrom = deg2rad($lat1);
        $lonFrom = deg2rad($long1);
        $latTo = deg2rad($lat2);
        $lonTo = deg2rad($long2);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(
            pow(sin($latDelta / 2), 2)
            + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)
        ));

        return $angle * CalculDistanceImpl::EARTH_RADIUS;
    }
    
    public function calculDistanceTrajet(Array $parcours): int {
        array_reduce($parcours, function($gps1, $gps2) {
            return this.calculDistance2PointsGPS($gps1->latitude, $gps1->longitude, $gps2->latitude, $gps2->longitude);
        });
    }

    public static function getInstance(): CalculDistanceImpl {
        if (this::$instace === NULL) {
            this::$instance = new CalculDistanceImpl();
        }
        return this::instance;
    }

}

?>