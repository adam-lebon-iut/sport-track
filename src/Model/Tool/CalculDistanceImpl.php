<?php

namespace App\Model\Tool;

require_once('ICalculDistance.php');

class CalculDistanceImpl implements ICalculDistance {

    private static $instance = NULL;

    const EARTH_RADIUS = 6371e3;

    public function calculDistance2PointsGPS(float $lat1, float $long1, float $lat2, float $long2): float {
        $latFrom = deg2rad($lat1);
        $lonFrom = deg2rad($long1);
        $latTo = deg2rad($lat2);
        $lonTo = deg2rad($long2);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(
            pow(sin($latDelta / 2), 2)
            + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)
        ));

        return $angle * CalculDistanceImpl::EARTH_RADIUS;
    }
    
    public function calculDistanceTrajet(Array $parcours): float {
        $distance = 0;
        for ($i = 1 ; $i < count($parcours); $i++) {
            $distance += $this->calculDistance2PointsGPS(
                $parcours[$i-1]->latitude,
                $parcours[$i-1]->longitude,
                $parcours[$i]->latitude,
                $parcours[$i]->longitude
            );
        }
        return floor($distance);
    }

    public static function getInstance(): CalculDistanceImpl {
        if (!isset(self::$instance)) {
            self::$instance = new CalculDistanceImpl();
        }
        return self::$instance;
    }

}

?>