<?php

namespace App\Model\Tool;

use App\Core\Sanitize;
use App\Model\Activite;
use App\Model\Donnee;

class JSONFactory
{

    /**
     * Transforme une intance d'Activite en json avec ses données associés
     *
     * @param Activity $activity
     * @return string Représentation JSON de l'activité et de ses données
     */
    public static function activityToJSON(Activite $activity): string
    {
        return json_encode([
            [
                'activity' => [
                    'date' => $activity->date->format('d/m/Y'),
                    'description' => $activity->description
                ],
                'data' => array_map(function($donnee) {
                    return [
                        'latitude' => $donnee->latitude,
                        'longitude' => $donnee->longitude,
                        'altitude' => $donnee->altitude,
                        'time' => $donnee->time->format('H:i:s'),
                        'cardio_frequency' => $donnee->cardioFrequency
                    ];
                }, $activity->getDonnees())
            ]
        ], JSON_PRETTY_PRINT);
    }

    /**
     * Lit un fichier JSON et en retourne la liste d'activités et de données
     *
     * @param string $file Chemin du fichier à lire
     * @return Array
     */
    public static function readFromFile(string $file): array
    {
        $donnees = file_get_contents($file);
        if ($donnees === null) {
            throw new Exception("Le fichier $file est introuvable");
        }

        $activites = json_decode($donnees, true);

        if ($activites === null) {
            throw new Exception("Le fichier json est invalide");
        }

        return array_map(function ($entry) {
            return [
                'activite' => self::createActivity($entry['activity']),
                'donnees' => self::createData($entry['data']),
            ];
        }, $activites);
    }

    /**
     * Tansforme un tableau associatif en instance de la classe Activite
     * Vérifie que tout les champs nécéssaires à la création d'une activité sont présent
     *
     * @throws Exception Si une des valeurs requise est absente
     * @param Array $activity Tableau avec les valeurs sources
     * @return Array<Activite>
     */
    private static function createActivity(array $activity): Activite
    {
        $missingFields = Sanitize::validateArray($activity, ['description', 'date']);
        if (!empty($missingFields)) {
            throw new \Exception('JSON invalide: les champs ' . array_join(' ', $missingFields) . ' sont manquants dans la section activity');
        }

        $instance = new Activite();
        $instance->description = $activity['description'];
        $instance->date = \DateTime::createFromFormat('d/m/Y', $activity['date']);

        return $instance;
    }

    /**
     * Transforme un talbeau de tableaux associatifs en tableau d'instances de la classe Donnee
     * Vérifie que tout les champs nécéssaires à la création d'une Donnee sont présent
     *
     * @throws Exception Si une des valeurs requise est absente
     * @param Array $datas Tableau de tableaux avec les valeurs sources
     * @return Array<Donnee>
     */
    private static function createData(array $datas, int $activiteId = null): array
    {
        return array_map(function ($data) {
            $missingFields = Sanitize::validateArray($data, ['latitude', 'longitude', 'altitude', 'time', 'cardio_frequency']);
            if (!empty($missingFields)) {
                throw new \Exception('JSON invalide: les champs ' . array_join(' ', $missingFields) . ' sont manquants dans la section activity');
            }

            $instance = new Donnee();
            $instance->latitude = $data['latitude'];
            $instance->longitude = $data['longitude'];
            $instance->altitude = $data['altitude'];
            $instance->time = \DateTime::createFromFormat('H:i:s', $data['time']);
            $instance->cardioFrequency = $data['cardio_frequency'];

            return $instance;
        }, $datas);
    }

}
