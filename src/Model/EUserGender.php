<?php

namespace App\Model;

/**
 * Énumeration qui définie le genre d'une personne
 * @see App\Model\Utilisateur
 * 
 * @author Adam LE BON <lebon.adam@gmail.com>
 */
class EUserGender {

    const Homme = 0;
    const Femme = 1;
    const Autre = 2;

}

?>