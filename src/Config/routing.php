<?php

namespace App\Config;

use App\Core\Routing\Route;
use App\Core\Routing\Router;
use App\Pages\Users\UsersController;
use App\Pages\Activity\ActivityController;

Router::add(new Route('', UsersController::class, 'login'));
Router::add(new Route('user/login', UsersController::class, 'login'));
Router::add(new Route('user/login', UsersController::class, 'loginPost', 'POST'));
Router::add(new Route('user/logout', UsersController::class, 'logout'));
Router::add(new Route('user/register', UsersController::class, 'register'));
Router::add(new Route('user/register', UsersController::class, 'registerPost', 'POST'));
Router::add(new Route('user/edit', UsersController::class, 'edit'));
Router::add(new Route('user/edit', UsersController::class, 'editPost', 'POST'));

Router::add(new Route('activity/list', ActivityController::class, 'list'));
Router::add(new Route('activity/import', ActivityController::class, 'import', 'POST'));
Router::add(new Route('activity/export/:id', ActivityController::class, 'export'));
Router::add(new Route('activity/delete/:id', ActivityController::class, 'delete'));

?>