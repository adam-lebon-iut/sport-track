<?php

// Force le typage stricte des variables
declare(strict_types=1);

// Affichage de toute les erreur
ini_set('display_errors', 'On');
error_reporting(E_ALL);

define('ROOT_DIR', __DIR__.'/');
define('PUBLIC_DIR', ROOT_DIR.'public');

session_start();

// Création d'un autoloader
// Cela nous permettra d'avoir un code plus propre en utilisant les namespaces
spl_autoload_register(function ($class) {
    // Toute les classes devront avoit des namespaces en App\...
    $prefix = 'App\\';
    
    $len = strlen($prefix);
    // Si la classe demandé est dans un autre namespace que App\
    if (strncmp($prefix, $class, $len) !== 0) {
        // Alors on ne fait rien
        return;
    }
    
    $relative_class = substr($class, $len);
    
    // Le chemin du fichier doit correspondre au namespace
    $file = ROOT_DIR . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($file)) {
        require_once $file;
    } else {
        throw new Exception("La fichier '$file' pour la classe '$class' est introuvable");
    }
});

use App\Core\Request;
use App\Core\FrontController;

if (!isset($_SESSION['notifications'])) {
    $_SESSION['notifications'] = [];
}

/**
 * On appelle le contrôleur de facade, celui ci va:
 * 1. Determiner le controlleur à appeller en fonctino des routes
 * 2. Instancier le bon controlleur et sa méthode
 * 3. Stocker le rendu du controlleur dans la variable $_SESSION['html_body']
 * 4. Stocket le titre de la pas dans la variable $_SESSION['html_title']
 */
(new FrontController())->render();

/**
 * Template HTML
 */
?>
<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title><?= $_SESSION['html_title'] ?? 'Sport track' ?></title>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="static/img/faviconblue.png" />
        <link rel="stylesheet" href="./static/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    </head>
    
    <body>
        <?php
        /**
         * On affiche les notifications et on les supprimes de la session pour qu'elles ne s'affichent plus
         */
        foreach ($_SESSION['notifications'] as $notif) {
            print '<div class="notification">' . $notif . '</div>';
        }
        unset($_SESSION['notifications']);
        
        /**
         * La variable de session html_body doit contenir le contenue du body généré par la vue
         */
        print ($_SESSION['html_body']);
        ?>
    </body>
</html>
