<?php

namespace App\Core;

class Sanitize
{

    /**
     * Désinfecte une string en se protegant contre les attaques XSS
     *
     * @param string $userInput chaine à desinfecter
     * @return string chaine sans risques
     */
    public static function XSS(string $userInput): string
    {
        // TODO
        return $userInput;
    }

    /**
     * Permet de vérifier si un tableaux contient bien tout les champs demandés
     *
     * @param array $toValidate Tableau à vérifier
     * @param array $requiredFields Contient les nom de tout les champs qui doivent être présent dans le premier argument
     * @return array Renvoie la liste des champs absents dans $toValidate
     */
    public static function validateArray(array $toValidate, array $requiredFields)
    {
        $notFound = [];

        foreach ($requiredFields as $key) {
            if (!isset($toValidate[$key]) || $toValidate[$key] === '') {
                $notFound[] = $key;
            }
        }

        return $notFound;
    }

    public static function validateLength(string $chaine, int $minLength, int $maxLength)
    {
        $len = strlen($chaine);
        if ($len <= $minLength) {
            return -1;
        } elseif($len >= $maxLength) {
            return 1;
        } else {
            return 0;
        }
    }
}
