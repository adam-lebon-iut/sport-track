<?php

namespace App\Core\Routing;

use App\Core\Request;

class Route
{

    /**
     * Url correspondant à la route
     *
     * @var string
     */
    public $path;

    /**
     * Classe du controller associé à la route
     *
     * @var mixed
     */
    public $controller;

    /**
     * Nom de la méthode à appeller dans le controlleur
     *
     * @var string
     */
    public $method;

    /**
     * Liste des arguments dynamic à parser dans l'url
     *
     * @var array<string>
     */
    public $arguments = [];

    /**
     * GET / POST
     *
     * @var string
     */
    public $requestMethod;

    public function __construct(string $path, $controller, string $method, string $reqMethod = 'GET')
    {
        $this->path = $path;
        $this->controller = $controller;
        $this->method = $method;
        $this->requestMethod = $reqMethod;
    }

    /**
     * Vérifie si une url correspont à la route courante
     * Prend en compte des paramètre dans l'url comme /activity/delete/:id où la valeur de :id
     * sera passé en paramètre du controlleur
     *
     * @param string $method
     * @param string $request
     * @return boolean True si la route correspond à l'url et si la méthode HTTP est la bonne
     */
    public function match(string $method, string $url)
    {
        if ($method !== $this->requestMethod) {
            return FALSE;
        }

        $url = trim($url, '/');
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        $regex = "#^$path$#i";
        $matches = [];

        if (!preg_match($regex, $url, $this->arguments)) {
            return FALSE;
        }

        array_shift($this->arguments);
        return TRUE;
    }
}
