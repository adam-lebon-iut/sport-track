<?php

namespace App\Core\Routing;

use App\Core\Exceptions\E404;
use App\Core\Request;
use App\Core\Routing\Route;

class Router
{

    /**
     * @var Array<Route>
     */
    private static $routes = [];

    public static function add(Route $route)
    {
        self::$routes[] = $route;
    }

    public static function match(string $method, string $url): Route
    {
        foreach (self::$routes as $route) {
            if ($route->match($method, $url)) {
                return $route;
            }
        }

        throw new E404($url);
    }

}
