<?php

namespace App\Core;

use App\Core\Routing\Route;
use App\Core\Routing\Router;
use App\Pages\Error\ErrorController;
use App\Core\Exceptions\E404;
/**
 * Controlleur de facade
 * Prend la reqûete dans son constructeur et utilise les routes
 * pour appeller le contrôlleur associé
 */
class FrontController {

    /**
     * Paramètre url passé dans la requête
     * Il sert à trouver la route correspondante
     *
     * @var string
     */
    private $url;

    /**
     * Route correspondante à l'url demandé
     *
     * @var Route
     */
    private $route;

    public function __construct() {
        $this->url = $_GET['url'] ?? '/';

        // Ce fichier déclare toute les routes du site
        require_once(ROOT_DIR . 'Config/routing.php');

        try {
            $this->route = Router::match($_SERVER['REQUEST_METHOD'], $this->url);
        } catch(E404 $exception) {
            $this->route = new Route($this->url, ErrorController::class, 'e404');
        }
    }

    /**
     * Appelle le méthode du controlleur correspondant à l'url demandé
     *
     * @return void
     */
    public function render() {
        $controller = $this->route->controller;
        $method = $this->route->method;

        ob_start();
            $instance = new $controller();
            call_user_func_array(array($instance, $method), $this->route->arguments);
        $_SESSION['html_body'] = ob_get_clean();
    }
    
}

?>