<?php

namespace App\Core\Exceptions;

use App\Core\Request;

class E404 extends \Exception {
    public function __construct (Request $request) {
        die("La page $request->requestedUri est introuvable");
    }
}
