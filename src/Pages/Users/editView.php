<div class="container">
	<h1>Éditer mes informations</h1>

	<hr>

	<form action="" method="POST">
		<div class="input-group">
				<label for="prenom">Nom</label>
				<input value="<?= $user->nom; ?>" type="text" name="nom" id="nom" placeholder="Dupont" required>
		</div>
			
		<div class="input-group">
				<label for="prenom">Prénom</label>
				<input value="<?= $user->prenom; ?>" type="text" name="prenom" id="prenom" placeholder="Jean" required>
		</div>
			
		<div class="input-group">
				<label for="ddn">Date de naissance</label>
				<input value="<?= $user->ddn->format('Y-m-d'); ?>" type="date" name="ddn" id="ddn" required>
		</div>
			
		<div class="input-group">
			<label>Genre</label>
			<span>
				<label for="h">Homme</label>
				<input type="radio" name="gender" value="0" id="h" <?= $user->genre === 0 ? 'checked':''; ?>>

				<label for="f">Femme</label>
				<input type="radio" name="gender" value="1" id="f" <?= $user->genre === 1 ? 'checked':''; ?>>

				<label for="o">Autre</label>
				<input type="radio" name="gender" value="2" id="o" <?= $user->genre === 2 ? 'checked':''; ?>>
			</span>
		</div>
		<div class="input-group">
			<label for="taille">Taille (en cm)</label>
			<input type="number" name="taille" id="taille" value="<?= $user->taille; ?>" required>
		</div>

		<div class="input-group">
			<label for="poids">Poids (en kg)</label>
			<input type="number" name="poids" id="poids" value="<?= $user->poids; ?>" required>
		</div>

		<div class="input-group">
			<label for="email">Adresse mail</label>
			<input type="email" name="email" id="email" placeholder="jean.dupont@yopmail.org" value="<?= $user->email; ?>" required>
		</div>

		<div class="input-group">
			<label for="mdp">Mot de passe</label>
			<input type="password" name="mdp" id="mdp" required>
		</div>

		<div class="input-group">
			<label for="mdpbis">Validez votre mot de passe</label>
			<input type="password" name="mdpbis" id="mdpbis" required>
		</div>

		<a href="?url=/activity/list"><input type="button" value="Annuler"></a>
		<input type="submit" value="Enregistrer">
	</form>
</div>