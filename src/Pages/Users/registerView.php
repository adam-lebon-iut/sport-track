<?php $_SESSION['html_title'] = 'Connexion au Compte';?>

<div class="container">
    <h1>Création de votre compte</h1>

    <hr/>

    <form action="" method="POST">
        <div class="input-group">
                <label for="prenom">Nom</label>
                <input type="text" name="nom" id="nom" placeholder="Dupont" required>
        </div>
        
        <div class="input-group">
                <label for="prenom">Prénom</label>
                <input type="text" name="prenom" id="prenom" placeholder="Jean" required>
        </div>
        
        <div class="input-group">
                <label for="ddn">Date de naissance</label>
                <input type="date" name="ddn" id="ddn" required>
        </div>
        
        <div class="input-group">
            <label>Genre</label>
            <span>
                <label for="h">Homme</label>
                <input type="radio" name="gender" value="h" id="h" required>

                <label for="f">Femme</label>
                <input type="radio" name="gender" value="f" id="f"required>

                <label for="o">Autre</label>
                <input type="radio" name="gender" value="o" id="o" checked required>
            </span>
        </div>
        <div class="input-group">
            <label for="taille">Taille (en cm)</label>
            <input type="number" name="taille" id="taille" value="170" required>
        </div>

        <div class="input-group">
            <label for="poids">Poids (en kg)</label>
            <input type="number" name="poids" id="poids" value="70" required>
        </div>

        <div class="input-group">
            <label for="mel">Adresse mail</label>
            <input type="email" name="email" id="email" placeholder="jean.dupont@yopmail.org" required>
        </div>

        <div class="input-group">
            <label for="mdp">Mot de passe</label>
            <input type="password" name="mdp" id="mdp" required>
        </div>

        <div class="input-group">
            <label for="mdpbis">Validez votre mot de passe</label>
            <input type="password" name="mdpbis" id="mdpbis" required>
        </div>

        <input type="submit" value="Enregistrer">
    </form>
</div>