<?php

namespace App\Pages\users;

use App\Core\Request;
use App\Core\Sanitize;
use App\Model\Utilisateur;
use App\DAO\UtilisateurDAO;

class UsersController
{

    /**
     * Instance du DAO des utilisateurs
     *
     * @var UtilisateurDAO
     */
    private $usersDAO;

    public function __construct()
    {
        $this->usersDAO = UtilisateurDAO::getInstance();
    }

    /**
     * GET /user/login 
     * Affiche simplement la vue de connexion et redirige l'utilisateur si il est déja connecté
     */
    public function login()
    {
        if (isset($_SESSION['id']) && sizeof($this->usersDAO->findWhere('id = ?', [$_SESSION['id']])) === 1) {
            header('Location: ?url=/activity/list');
            exit();
        }
        require 'loginView.php';
    }

    /**
     * POST /user/login
     * Methode de soumission du formulaire
     * Traite l'identification de l'utilisateur et le redirige vers la bonne page
     */
    public function loginPost()
    {
        // Si l'id est déja définie
        if (isset($_SESSION['id'])) {
            // On vérifie que l'utilisateur existe bien
            if (sizeof($this->usersDAO->findWhere('id = ?', [$_SESSION['id']])) === 1) {
                // Si oui on redirige l'utilisateur vers son profile
                header('Location: ?url=/activity/list');
                exit();
            } else {
                // Sinon on détruit la variable car sa valeur est invalide
                unset($_SESSION['id']);
            }
        } elseif (isset($_POST['email']) && isset($_POST['mdp'])) {
            $match = $this->usersDAO->findWhere('email=? AND mdp=?', [
                $_POST['email'],
                sha1($_POST['mdp'])
            ]);

            if (count($match) === 1) {
                $_SESSION['id'] = $match[0]->id;
                header('Location: ?url=/activity/list');
                exit();
            } else {
                $_SESSION['notifications'][] = 'Mauvais email / Mot de passe';
            }
        } else {
            $_SESSION['notifications'][] = 'Erreur, un des champs email ou password est absent';
        }
        header('Location: ?url=/user/login');
        exit();
    }

    /**
     * GET /user/logout
     * Deconnecte l'utilisateur
     */
    public function logout()
    {
        // On supprime simplement la variable de session
        unset($_SESSION['id']);
        header('Location: ?url=/user/login');
        exit();
    }

    /**
     * GET /user/register
     * Affiche simplement la vue d'inscription
     */
    public function register()
    {
        require('registerView.php');
    }

    /**
     * POST /user/register
     * Méthode de soumission du formulaire d'enregistrement
     * Vérifie les différents critère et redirige l'utilisateur vers la bonne page
     */
    public function registerPost()
    {
        try {
            /**
             * On vérifie que le formulaire est en ordre
             */
            $this->validateUserInfos($_POST);

            /**
             * On vérifie qu'un autre utilisateur n'a pas la même email
             */
            if (count($this->usersDAO->findWhere('email = ?', [$_POST['email']])) > 0) {
                throw new \Exception('Un utilisateur avec ce compte existe déja');
            }

            /**
             * Si tout est bon on insert le nouvelle utilisateur
             */
            $user = new Utilisateur();
            $user->nom = $_POST['nom'];
            $user->prenom = $_POST['prenom'];
            $user->ddn = new \DateTime($_POST['ddn']);
            $user->genre = $_POST['gender'];
            $user->taille = $_POST['taille'];
            $user->poids = $_POST['poids'];
            $user->email = $_POST['email'];
            $user->setPassword($_POST['mdp']);
    
            $this->usersDAO->insert($user);
            header('Location: ?url=/user/login');
        } catch (\Exception $e) {
            /**
             * Sinon on affiche les erreurs de l'utilisateur
             */
            $_SESSION['notifications'][] = $e->getMessage();

            /**
             * Bidouillage: permet de retourner au formulaire sans remplir tout les champs
             * La meilleur méthode aurait été de rediriger l'utilisateur vers le formulaire
             * et de préremplir les champs avec les dernière valeurs rentrées
             */
            die('<script>history.back(-1);</script>');
        }
        exit();
    }

    /**
     * GET /user/edit
     * Affiche le formulaire d'édition de l'utilisateur
     */
    public function edit()
    {
        $user = $this->getCurrentUser();
        require('editView.php');
    }

    /**
     * POST /user/edit
     * Verifie et enregistre les nouvelles informations de l'utilisateur en BDD
     * Le redirige vers la page d'édition si toute les informations sont valides
     */
    public function editPost()
    {

        
        try {
            // On vérifie qu'un utilisateur est connecté et on récupère son instance
            $currentUser = $this->getCurrentUser();

            // On vérifie que toute les informations sont correctes
            $this->validateUserInfos($_POST);

            // On redéfinie toute les valeurs (même si elles n'ont pas changé);
            $currentUser->nom = $_POST['nom'];
            $currentUser->prenom = $_POST['prenom'];
            $currentUser->ddn = new \DateTime($_POST['ddn']);
            $currentUser->genre = $_POST['gender'];
            $currentUser->taille = $_POST['taille'];
            $currentUser->poids = $_POST['poids'];
            $currentUser->email = $_POST['email'];
            $currentUser->setPassword($_POST['mdp']);

            $this->usersDAO->update($currentUser);

            header('Location: ?url=/activity/list');
            exit();
        } catch (\Exception $e) {
            $_SESSION['notifications'][] = $e->getMessage();
            die('<script>history.back(-1);</script>');
        }

        exit();
    }

    /**
     * Vérifie l'exactitude des informations rentrés dans le formulaire
     * Déclenche une exception si une erreur à été faite par l'utilisateur
     *
     * @param array $infos tableau de valeurs à vérifier
     * @throws Exception Si une erreur à été faite
     * @return void
     */
    private function validateUserInfos(array $infos)
    {
        $messages = [];
        
        /**
         * Préviens des injections XSS
         */
        foreach ($infos as $key => $value) {
            $info[$key] = Sanitize::XSS($value);
        }

        /**
         * Détecte les champs non remplit
         */
        $missingFields = Sanitize::validateArray(
            $infos,
            ['nom', 'prenom', 'ddn', 'gender', 'taille', 'poids', 'email', 'mdp', 'mdpbis']
        );

        foreach ($missingFields as $field) {
            $messages[] = "Le champs $field ne peut pas être vide";
        }

        if (empty($missingFields)) {
            /**
             * Vérifie la correspondance des mot de passe
             */
            if ($infos['mdp'] !== $infos['mdpbis']) {
                $messages[] = 'Les mots de passe ne correspondent pas';
            }
            
    
            /**
             * Vérification de la longueur du mot de passe
             */
            $mdpValid = Sanitize::validateLength($infos['mdp'], 8, 20);
            if ($mdpValid === -1) {
                $messages[] = 'Mot de passe trop petit';
            } elseif($mdpValid === 1) {
                $messages[] = 'Mot de passe trop long';
            }
        }
        

        if (!empty($messages)) {
            throw new \Exception(join('<br>', $messages));
        }
    }

    private function getCurrentUser(): Utilisateur
    {
        if (!isset($_SESSION['id'])) {
            header('Location: ?url=/user/login');
            exit();
        }
        
        $matche = $this->usersDAO->findWhere('id = ?', [$_SESSION['id']]);

        if (sizeof($matche) !== 1) {
            unset($_SESSION['id']);
            header('Location: ?url=/user/login');
            exit();
        }

        return $matche[0];
    }
}
