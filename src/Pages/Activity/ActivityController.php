<?php

namespace App\Pages\Activity;

use App\DAO\UtilisateurDAO;
use App\DAO\ActiviteDAO;
use App\DAO\DonneeDAO;

use App\Model\Utilisateur;
use App\Model\Tool\JSONFactory;

class ActivityController
{

    /**
     * @var UtilisateurDAO
     */
    private $userDAO;

    /**
     * @var ActiviteDAO
     */
    private $activiteDAO;

    /**
     * @var DonneeDAO
     */
    private $donneeDAO;

    public function __construct()
    {
        $this->userDAO = UtilisateurDAO::getInstance();
        $this->activiteDAO = ActiviteDAO::getInstance();
        $this->donneeDAO = DonneeDAO::getInstance();
    }

    /**
     * GET /activity/list
     * Rend la vue de gestion des activités, il pourra:
     * - Envoyer des fichier JSON afin de les importer
     * - Consulter ses activités
     * - Supprimer des activites
     * - Télécharger des activités au format JSON
     * L'utilisateur doit être connecté sinon il sera redirigé
     */
    function list()
    {
        $activities = $this->getCurrentUser()->getActivities();
        $user = $this->getCurrentUser();
        require 'manageActivities.php';
    }

    /**
     * POST /activity/import
     * Permet d'uploader un fichier JSON afin de l'importer dans la BDD
     */
    public function import()
    {
        try {
            if (!isset($_FILES['fichier'])) {
                throw new \Exception('Le champ fichier est absent');
            }
    
            if ($_FILES['fichier']['type'] !== 'application/json' || preg_match('/.json$/', $_FILES['fichier']['name']) === 0) {
                throw new \Exception('Le fichier doit être un au format JSON');
            }
            // La JSONFactory nou génére les classes Activite et Donnee depuis le JSON
            $import = JSONFactory::readFromFile($_FILES['fichier']['tmp_name']);

            // Pour chaque activty
            foreach($import as $entry) {
                // On la sauvegarde en ajoutant la clef étrangère vers Utilisateur
                $entry['activite']->utilisateurId = $_SESSION['id'];
                $this->activiteDAO->insert($entry['activite']);
    
                // Et pour chaque donnee de l'activite
                foreach($entry['donnees'] as $data) {
                    // On la sauvegarde en ajoutant la clef étrangère vers l'activité
                    $data->activiteId = $entry['activite']->id;
                    $this->donneeDAO->insert($data);
                }
            }

            // Si tout s'est bien passé on redirige l'utilisateur vers la liste des activites
            // avec une notification
            $_SESSION['notifications'][] = 'Le fichier à bien été importé !';
        } catch(\Exception $e) {
            // Sinon on le redirige avec le message d'erreur
            $_SESSION['notifications'][] = $e->getMessage();
        }
        header('Location: ?url=/activity/list');
        exit();
    }

    /**
     * GET /activity/export/$id
     * Permet de télécharger le fichier JSON d'une activité
     * 
     * @param integer $id Identifiant de l'activité
     */
    public function export(int $id)
    {
        try {
            // On vérifie si l'activité demandé existe bien
            $activity = $this->activiteDAO->findWhere('id = ?', [$id]);
            if (count($activity) !== 1) {
                http_response_code(400);
                throw new \Exception("L'activité que vous essayez d'exporter n'existe pas");
            }
    
            // On vérifie que l'utilisateur possède bien l'activité
            $currentUser = $this->getCurrentUser();
            if ($currentUser->id !== $activity[0]->utilisateurId) {
                http_response_code(401);
                throw new \Exception("L'activité que vous essayez d'exploter ne vous apartient pas");
            }
    
            // Si tout va bien on peut lui donner le fichier json
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"" . $activity[0]->date->format('d-m-Y') . ".json\""); 
            print JSONFactory::activityToJSON($activity[0]);
            die();
        } catch (\Exception $e) {
            $_SESSION['notifications'][] = $e->getMessage();
        }
        header('Location: ?url=/activity/list');
        exit();
    }

    /**
     * GET /activity/delete/$id
     * Permet de supprimer une activité et toute ses données associés
     *
     * @param integer $id Identifiant de l'actiité
     */
    public function delete(int $id)
    {
        try {
            // On vérifie que l'activité demandé existe bien
            $activite = $this->activiteDAO->findWhere('id = ?', [$id]);
            if (empty($activite)) {
                http_response_code(400);
                throw new \Exception("L'activité que vous essayé de supprimer n'existe pas");
            }
            
            $currentUser = $this->getCurrentUser();
            // On vérifie si l'activité à supprimer appartient bien à l'utilisateur courant
            if ($activite[0]->utilisateurId !== $currentUser->id) {
                http_response_code(401);
                throw new \Exception("Vous n'êtes pas le propriétaire de cette activité !");
            }

            // Si tout fonctionne, on supprime l'activité demandé, les données sont automatiquement supprimés par le DAO
            $this->activiteDAO->delete($activite[0]->id);
            $_SESSION['notifications'][] = "L'activité à bien été supprimé";

        } catch (\Exception $e) {
            $_SESSION['notifications'][] = $e->getMessage();
        }
        
        header('Location: ?url=/activity/list');
        exit();
    }

    /**
     * Fonction interne au controlleur permettant de rapidement récupérer l'instance de l'utilisateur
     * Si l'utilisateur n'est pas connecté, il est alors redirigé sur la page de connexion
     *
     * @return Utilisateur
     */
    private function getCurrentUser(): Utilisateur
    {
        if (!isset($_SESSION['id'])) {
            header('Location: ?url=/user/login');
            exit();
        }

        $matche = $this->userDAO->findWhere('id = ?', [$_SESSION['id']]);

        if (sizeof($matche) !== 1) {
            unset($_SESSION['id']);
            $_SESSION['notifications'][] = 'Vous devez vous connecter afin d\'acceder à cette page';
            http_response_code(401);
            header('Location: ?url=/user/login');
            exit();
        }

        return $matche[0];
    }
}
