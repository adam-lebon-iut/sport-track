<link rel="stylesheet" href="static/style-files.css" />
<div class="container">
    <b>Bonjour <a href="?url=/user/edit"><?= $user->prenom . ' ' . $user->nom; ?></a> (<a href="?url=/user/logout">Se deconnecter</a>)</b>
    <h1>Ajouter des fichiers</h1>
    <hr/>
    <form class="activity" action="?url=/activity/import" method="POST" enctype="multipart/form-data">
        <div class="input-group">
            <label for="fichier">Choisissez un fichier .json:</label>
            <input type="file" id="fichier" name="fichier">
        </div>
        <input type="submit" value="Importer">
    </form>

    <h1>Gérer les activités</h1>
    <hr>
    
    <?php if (count($activities) === 0): ?>
        <i>Vous n'avez aucune activité, pour en ajouter une, utilisez le formulaire d'importation.</i>
    <?php endif; ?>
    
    <?php foreach($activities as $activity): ?>
        <div class="activity">
            <div class="row">
                <div>
                    <b><?= $activity->description; ?>:</b>
                    &nbsp;
                    <?= $activity->date->format('d/m/Y') ?>
                    &nbsp;
                    (<?= $activity->getDistance(); ?>Km)
                </div>
                
                <div>
                    <a href="?url=/activity/export/<?= $activity->id; ?>" title="Exporter">💾</a>
                    &nbsp;
                    <a href="?url=/activity/delete/<?= $activity->id; ?>" title="Supprimer">🗑</a>
                </div>
            </div>
            <div class="row">
                <div>💓 Min: <?= $activity->getMinCardio(); ?> | Moyen: <?= $activity->getAverageCardio(); ?> | Max: <?= $activity->getMaxCardio(); ?></div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
