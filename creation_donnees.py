# -*- coding: utf-8 -*-
import os
from datetime import datetime
from random import randint,uniform

#Incrémentation de l'heure
def incrTime(time):
    heures = time[0]
    minutes = time[1]
    secondes = time[2]

    if (secondes > 54):
        secondes -= 55
        if (minutes == 59):
            minutes = 0
            if (heures == 23):
                heures = 0
            else:
                heures += 1
        else:
            minutes += 1
    else:
        secondes += 5

    return [heures,minutes,secondes]


#Generation de la date
annee = randint(2000, 2018)
mois = randint(1, 12)
jour = randint(1, 28)

#Generation de la description
places = ("IUT","RU","Mairie","Port","ENSIBS","Kerlann")
depart = places[randint(0,len(places)-1)]
arrivee = depart
while (arrivee==depart):
    arrivee = places[randint(0,len(places))-1]

#Affichage de la date et description
print'['
print '  {'
print '    "activity":{'
print '      "date":"%02d/%02d/%d",' % (jour,mois,annee)
print '      "description": "%s -> %s"' % (depart,arrivee)
print '    },'
print '    "data":['


#Generation des lignes à rentrer
nbLignes = randint(5,50)
time = [randint(0,23),randint(0,59),randint(0,59)]
cardio = randint(40,200)
latitude = float(format(uniform(-90,90), '.7g'))
longitude = float(format(uniform(0,180), '.7g'))
altitude = randint(-100,7000)

for i in range(0,nbLignes):
    #Generation des données de la ligne courante
    time = incrTime(time)
    cardio += randint(-20,+20)
    latitude += uniform(-0.00009,0.00009)
    longitude += uniform(-0.00009,0.00009)
    altitude += int(round(uniform(-1,1)))
   
    #Affichage des données
    if (i!=nbLignes-1):
        print '{"time":"%02d:%02d:%02d","cardio_frequency":%d,"latitude":%06f,"longitude":%06f,"altitude":%d},' % (time[0],time[1],time[2],cardio,latitude,longitude,altitude)
    #Si on est à la dernière ligne on ne met pas de virgule
    else:
        print '{"time":"%02d:%02d:%02d","cardio_frequency":%d,"latitude":%06f,"longitude":%06f,"altitude":%d}' % (time[0],time[1],time[2],cardio,latitude,longitude,altitude)

print '    ]'
print '  }'
print ']'